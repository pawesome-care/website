import React, { useState } from 'react';
import NavBar from '../components/Navbar/NavBar';
import { useDocTitle } from '../components/CustomHook';
import axios from 'axios';
import Notiflix from 'notiflix';

function UserProfile() {
  useDocTitle('Pawsome Care');
  const [editMode, setEditMode] = useState(false);
  const [showSettings, setShowSettings] = useState(false);
  const [showChangePassword, setShowChangePassword] = useState(false);
  const [showMyBookings, setShowMyBookings] = useState(false);
  const [userData, setUserData] = useState({
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@example.com',
  });
  const [passwordData, setPasswordData] = useState({
    currentPassword: '',
    newPassword: '',
    confirmPassword: '',
  });
  const [isPasswordEditMode, setPasswordEditMode] = useState(false);

  const handleEditClick = () => {
    setEditMode(!editMode);
  };

  const handleSettingsClick = () => {
    setShowSettings(!showSettings);
    setShowChangePassword(false);
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setUserData({
      ...userData,
      [name]: value,
    });
  };

  const handlePasswordInputChange = (e) => {
    const { name, value } = e.target;
    setPasswordData({
      ...passwordData,
      [name]: value,
    });
  };

  const handleSavePassword = () => {
    // Add your logic to save the new password here
    // You can compare the current password with the stored one and save the new password if it matches
    // You should also handle validation and error messages
    console.log('Saving new password:', passwordData.newPassword);
    setPasswordEditMode(false);
  };

  const handleCancelPassword = () => {
    setPasswordEditMode(false);
    setPasswordData({
      currentPassword: '',
      newPassword: '',
      confirmPassword: '',
    });
  };

  const handleShowMyBookings = () => {
    setShowMyBookings(!showMyBookings);
  };

  return (
    <>
      <div>
        <NavBar />
      </div>
      <div className="flex justify-center items-center mt-8 bg-white py-12 lg:py-8 bg-gray-100">
        <div className="container mx-auto my-8 px-4 lg:px-20">
          <div className="flex">
            <div className="w-2/3 p-6 bg-white rounded-lg shadow-lg">
              <h1 className="text-2xl font-bold mb-4">User Profile</h1>
              <div className="mb-4">
                <label className="block text-gray-600">First Name:</label>
                {editMode ? (
                  <input
                    type="text"
                    name="firstName"
                    value={userData.firstName}
                    onChange={handleInputChange}
                    className="border rounded-lg px-3 py-2 w-full focus:outline-none focus:ring focus:border-blue-500"
                  />
                ) : (
                  <p className="text-gray-800">{userData.firstName}</p>
                )}
              </div>
              <div className="mb-4">
                <label className="block text-gray-600">Last Name:</label>
                {editMode ? (
                  <input
                    type="text"
                    name="lastName"
                    value={userData.lastName}
                    onChange={handleInputChange}
                    className="border rounded-lg px-3 py-2 w-full focus:outline-none focus:ring focus:border-blue-500"
                  />
                ) : (
                  <p className="text-gray-800">{userData.lastName}</p>
                )}
              </div>
              <div className="mb-4">
                <label className="block text-gray-600">Email:</label>
                {editMode ? (
                  <input
                    type="email"
                    name="email"
                    value={userData.email}
                    onChange={handleInputChange}
                    className="border rounded-lg px-3 py-2 w-full focus:outline-none focus:ring focus:border-blue-500"
                  />
                ) : (
                  <p className="text-gray-800">{userData.email}</p>
                )}
              </div>
              <button
                className={`mt-6 ${
                  editMode ? 'bg-blue-500 hover:bg-blue-700' : 'bg-gray-500 hover:bg-gray-700'
                } text-white font-bold py-2 px-4 rounded`}
                onClick={handleEditClick}
              >
                {editMode ? 'Save' : 'Edit'}
              </button>
              <button
                className={`mt-2 ${
                  !showMyBookings
                    ? 'bg-blue-500 hover:bg-blue-700'
                    : 'bg-gray-500 hover:bg-gray-700'
                } text-white font-bold py-2 px-4 rounded focus:outline-none`}
                onClick={handleShowMyBookings}
              >
                {showMyBookings ? 'Hide My Bookings' : 'My Bookings'}
              </button>
            </div>
            <div className="w-1/3 p-6 bg-white rounded-lg shadow-lg ml-4">
              {showSettings ? (
                <div>
                  <h2 className="text-lg font-semibold mb-4">Settings</h2>
                  {showChangePassword ? (
                    <div className="mt-4">
                      <h2 className="text-lg font-semibold mb-4">Change Password</h2>
                      <div className="mb-4">
                        <label className="block text-gray-600">Current Password:</label>
                        <input
                          type="password"
                          name="currentPassword"
                          value={passwordData.currentPassword}
                          onChange={handlePasswordInputChange}
                          className="border rounded-lg px-3 py-2 w-full focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                      <div className="mb-4">
                        <label className="block text-gray-600">New Password:</label>
                        <input
                          type="password"
                          name="newPassword"
                          value={passwordData.newPassword}
                          onChange={handlePasswordInputChange}
                          className="border rounded-lg px-3 py-2 w-full focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                      <div className="mb-4">
                        <label className="block text-gray-600">Confirm Password:</label>
                        <input
                          type="password"
                          name="confirmPassword"
                          value={passwordData.confirmPassword}
                          onChange={handlePasswordInputChange}
                          className="border rounded-lg px-3 py-2 w-full focus:outline-none focus:ring focus:border-blue-500"
                        />
                      </div>
                      <div className="flex"> {/* Insert Cancel button */}
                        <button
                          className="mr-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none"
                          onClick={handleSavePassword}
                        >
                          Save Password
                        </button>
                        <button
                          className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none"
                          onClick={handleCancelPassword}
                        >
                          Cancel
                        </button>
                      </div>
                    </div>
                  ) : (
                    <div className="mt-4">
                      <button
                        className="text-lg font-semibold mb-2"
                        onClick={() => {
                          setShowChangePassword(true);
                          setPasswordEditMode(true);
                        }}
                      >
                        Change Password
                      </button>
                    </div>
                  )}
                </div>
              ) : (
                <button
                  className="text-lg font-semibold text-blue-500"
                  onClick={handleSettingsClick}
                >
                  Settings
                </button>
              )}
            </div>
          </div>
          {showMyBookings && (
            <div className="mt-6 p-6 bg-white rounded-lg shadow-lg">
              <h2 className="text-xl font-semibold mb-4">My Bookings</h2>
              {/* Add your My Bookings content here */}
            </div>
          )}
        </div>
      </div>
    </>
  );
}

export default UserProfile;
