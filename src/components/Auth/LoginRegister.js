import React, { useState } from "react";
import { Link } from 'react-router-dom';
import {useDocTitle} from '../../components/CustomHook';
import NavBar from '../../components/Navbar/NavBar';
import overlayImage from '../../images/vet8.jpg';
// import emailjs from 'emailjs-com';
 import * as Components from './LoginRegStyles';

 function LoginRegister() {
     useDocTitle('Pawsome Care')

     const [signIn, toggle] = useState(true);

      return(
        <>
        <div>
            <NavBar />
        </div>
        <div className="flex justify-center items-center mt-8 w-full bg-white py-12 lg:py-6">
                <div className="container mx-auto my-8 px-4 lg:px-20" data-aos="zoom-in">
          <Components.Container>
              <Components.SignUpContainer signinIn={signIn} id="login-register">
                  <Components.Form>
                      <Components.Title>Create Account</Components.Title>
                      <Components.Input type='text' placeholder='Name' />
                      <Components.Input type='contact' placeholder='Contact Number' />
                      <Components.Input type='email' placeholder='Email' />
                      <Components.Input type='password' placeholder='Password' />
                      <Components.Input type='password' placeholder='Confirm Password' />
                      <Components.Button>Sign Up</Components.Button>
                  </Components.Form>
              </Components.SignUpContainer>
             {/* Wrap the right side content in a separate div */}
              <Components.SignInContainer signinIn={signIn}>
                   <Components.Form>
                       <Components.Title>Sign in</Components.Title>
                       <Components.Input type='email' placeholder='Email' />
                       <Components.Input type='password' placeholder='Password' />
                       <Link to="/forgot-password">Forgot your password?</Link>
                       <Components.Button>Sign In</Components.Button>
                   </Components.Form>
              </Components.SignInContainer>


              <Components.OverlayContainer signinIn={signIn}>
                  <Components.Overlay signinIn={signIn}>

                  <Components.LeftOverlayPanel signinIn={signIn}>
                        <img src={overlayImage} alt="Overlay Image"/>
                      <Components.GhostButton onClick={() => toggle(true)}>
                          Sign In
                      </Components.GhostButton>
                      </Components.LeftOverlayPanel>

                      <Components.RightOverlayPanel signinIn={signIn}>
                        <img src={overlayImage} alt="Overlay Image"/>
                            <Components.GhostButton onClick={() => toggle(false)}>
                                Sign Up
                            </Components.GhostButton> 
                      </Components.RightOverlayPanel>
  
                  </Components.Overlay>
              </Components.OverlayContainer>
          </Components.Container>
       
     
          </div>
          </div>
          </>
      )
 }

 export default LoginRegister;